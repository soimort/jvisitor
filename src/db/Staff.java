package db;

import java.util.UUID;

/**
 * Staff class (inherited from Person).
 * 
 * @author	Mort Yao
 * @since	2012-05-25
 * @see Person
 */
public class Staff extends Person {
    
    /**
     * Creates a new Staff.
     */
    public Staff() {
	
        setUuId(UUID.randomUUID());
        
    }
    
    /**
     * Creates a new Staff (with parameters).
     * 
     * @param name Name of the Staff
     */
    public Staff(String name) {
        
        setUuId(UUID.randomUUID());
        
        setName(name);
    
    }
    
    /**
     * Creates a new Staff (with parameters).
     * 
     * @param name Name of the Staff
     * @param idType ID type of the Staff
     * @param idNumber ID number of the Staff
     */
    public Staff(String name, String idType, String idNumber) {
        
        setUuId(UUID.randomUUID());
        
        setName(name);
        setIdType(idType);
        setIdNumber(idNumber);
        
    }
    
    /**
     * Creates a new Staff (with parameters).
     * 
     * @param uuId_str UUID string of the Staff
     * @param name Name of the Staff
     * @param idType ID type of the Staff
     * @param idNumber ID number of the Staff
     */
    public Staff(String uuId_str, String name, String idType, String idNumber) {
        
        setUuId(UUID.fromString(uuId_str));
        
        setName(name);
        setIdType(idType);
        setIdNumber(idNumber);
        
    }
    
}
