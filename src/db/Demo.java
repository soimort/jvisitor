package db;

// Example.
public class Demo {
    
    public static void main(String[] args) throws Exception {
        
        // Start the database connection.
        Database db = new Database("demo", "sa", "12", "group");
        
        // Get current settings.
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>");
        System.out.println("DATABASE_NAME: " + db.getName());
        System.out.println("DATABASE_PATH: " + db.getPath());
        System.out.println("CACHE_SIZE: " + db.getCacheSize());
        System.out.println("LOG: " + db.getLog());
        System.out.println("MULTI_THREADED: " + db.getMultiThreaded());
        System.out.println("QUERY_TIMEOUT: " + db.getQueryTimeout());
        System.out.println();
        
        // Set user password.
        db.setPassword("123456");
        
        // Close the existing database connection and restart a new connection.
        db = new Database("demo", "sa", "123456", "group", "AES", "FILE", true, 8192, 1, 1, 10000);
        
        // Get current settings.
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>");
        System.out.println("DATABASE_NAME: " + db.getName());
        System.out.println("DATABASE_PATH: " + db.getPath());
        System.out.println("CACHE_SIZE: " + db.getCacheSize());
        System.out.println("LOG: " + db.getLog());
        System.out.println("MULTI_THREADED: " + db.getMultiThreaded());
        System.out.println("QUERY_TIMEOUT: " + db.getQueryTimeout());
        System.out.println();
        
        // Set database settings.
        db.setCacheSize(16384);
        db.setLog(2);
        db.setMultiThreaded(0);
        db.setQueryTimeout(0);
        
        // Get current settings.
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>");
        System.out.println("CACHE_SIZE: " + db.getCacheSize());
        System.out.println("LOG: " + db.getLog());
        System.out.println("MULTI_THREADED: " + db.getMultiThreaded());
        System.out.println("QUERY_TIMEOUT: " + db.getQueryTimeout());
        System.out.println();
        
        // Get the objects of all staff.
        Staff[] s = db.getStaffAll();
        
        if (s.length == 0) {
            
            // Create a new Staff object and add it to the database.
            /*
            Staff s = new Staff("Mort", "PERSONNUMMER", "123456-7890");
            db.addStaff(s);
            */
            
            // Alternatively you can add new staff to the database without creating any Staff object.
            db.addNewStaff("Mort", "PERSONNUMMER", "123456-7890");
            db.addNewStaff("Momochan", "PERSONNUMMER", "234567-8901");
            
        }
        
        // Get the objects of all staff whose name start with "mo" (case-insensitive).
        s = db.getStaffByName("mo");
        
        if (s.length > 0) {
            
            // Get the information of staff.
            for (Staff i: s) {
                System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>");
                System.out.println("UUID: " + i.getUuId());
                System.out.println("Name: " + i.getName());
                System.out.println("ID Type: " + i.getIdType());
                System.out.println("ID Number: " + i.getIdNumber());
                System.out.println();
            }
            
            // Remove a staff from the database.
            db.removeStaff(s[s.length - 1]);
            
        }
        
        // Get the objects of all visitors.
        Visitor[] v = db.getVisitorsAll();
        
        if (v.length == 0 && s.length > 0) {
            
            // Create a new Visitor object (whose attendant is s[0]) and add it to the database.
            /*
            Visitor v = new Visitor("Spock", "PASSPORT", "Z00000001", "Enterprise", "Vulcan", s);
            db.addVisitor(v);
            */
            
            // Alternatively you can add new visitor to the database without creating any Visitor object.
            db.addNewVisitor("Spock", "PASSPORT", "Z00000001", "Enterprise", "Vulcan", s[0]);
            
        }
        
        // Get the objects of all visitors whose name start with "s" (case-insensitive).
	v = db.getVisitorsByName("s");
        
	if (v.length > 0) {
            
            // Get the information of visitors.
            for (Visitor i: v) {
                System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>");
                System.out.println("UUID: " + i.getUuId());
                System.out.println("Name: " + i.getName());
                System.out.println("ID Type: " + i.getIdType());
                System.out.println("ID Number: " + i.getIdNumber());
                System.out.println("Company: " + i.getCompany());
                System.out.println("Country: " + i.getCountry());
                System.out.println("Attendant UUID: " + i.getAttendant());
                System.out.println("Attendant Name: " + i.getAttendantStaff().getName());
                System.out.println("Date/Time: " + i.getTimestamp());
                System.out.println();
            }
            
            // Remove a visitor from the database.
            db.removeVisitor(v[v.length - 1]);
            
        }
        
        // Set user password.
        db.setPassword("12");
        
    }
    
}
