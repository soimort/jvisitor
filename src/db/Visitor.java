package db;

import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

/**
 * Visitor class (inherited from Person).
 * 
 * @author	Mort Yao
 * @since	2012-05-25
 * @see Person
 */
public class Visitor extends Person {
    
    /**
     * Company of the Visitor.
     */
    private String company;
    
    /**
     * Country of the Visitor.
     */
    private String country;
    
    /**
     * Attendant (Staff) of the Visitor.
     */
    private Staff attendantStaff;
    
    /**
     * UUID of the attendant of the Visitor.
     */
    private UUID attendant;
    
    /**
     * Timestamp of visit of the Visitor.
     */
    private Timestamp timestamp;
    
    /**
     * Creates a new Visitor.
     */
    public Visitor() {
        
        setUuId(UUID.randomUUID());
        
        setTimestamp(new Timestamp(new Date().getTime()));
        
    }
    
    /**
     * Creates a new Visitor (with parameters).
     * 
     * @param name Name of the Visitor
     * @param idType ID type of the Visitor
     * @param idNumber ID number of the Visitor
     * @param company Company of the Visitor
     * @param country Country of the Visitor
     * @param attendantStaff Attendant (Staff) of the Visitor
     */
    public Visitor(String name, String idType, String idNumber, String company, String country, Staff attendantStaff) {
        
        setUuId(UUID.randomUUID());
        
        setName(name);
        setIdType(idType);
        setIdNumber(idNumber);
        setCompany(company);
        setCountry(country);
        setAttendantStaff(attendantStaff);
        setAttendant(attendantStaff.getUuId());
        
        setTimestamp(new Timestamp(new Date().getTime()));
        
    }
    
    /**
     * Creates a new Visitor (with parameters).
     * 
     * @param uuId_str UUID string of the Visitor
     * @param name Name of the Visitor
     * @param idType ID type of the Visitor
     * @param idNumber ID number of the Visitor
     * @param company Company of the Visitor
     * @param country Country of the Visitor
     * @param attendantStaff Attendant (Staff) of the Visitor
     * @param timestamp Timestamp of the visit of the Visitor
     */
    public Visitor(String uuId_str, String name, String idType, String idNumber, String company, String country, Staff attendantStaff, Timestamp timestamp) {
        
        setUuId(UUID.fromString(uuId_str));
        
        setName(name);
        setIdType(idType);
        setIdNumber(idNumber);
        setCompany(company);
        setCountry(country);
        setAttendantStaff(attendantStaff);
        setAttendant(attendantStaff.getUuId());
        
        setTimestamp(timestamp);
        
    }

    /**
     * Returns the company of the Visitor.
     * 
     * @return Company
     */
    public String getCompany() {
        
        return company;
        
    }
    
    /**
     * Sets the company of the Visitor.
     * 
     * @param company Company
     */
    public void setCompany(String company) {
        
        this.company = company;
        
    }
    
    /**
     * Returns the country of the Visitor.
     * 
     * @return Country
     */
    public String getCountry() {
        
        return country;
        
    }
    
    /**
     * Sets the country of the attendant of the Visitor.
     * 
     * @param country Country
     */
    public void setCountry(String country) {
        
        this.country = country;
        
    }
    
    /**
     * Returns the attendant of the Visitor.
     * 
     * @return Attendant (Staff)
     */
    public Staff getAttendantStaff() {
        
        return attendantStaff;
        
    }
    
    /**
     * Sets the attendant of the Visitor.
     * 
     * @param attendantStaff Attendant (Staff)
     */
    public void setAttendantStaff(Staff attendantStaff) {
        
        this.attendantStaff = attendantStaff;
        
    }
    
    /**
     * Returns the UUID of the attendant of the Visitor.
     * 
     * @return UUID of the attendant
     */
    public UUID getAttendant() {
        
        return attendant;
        
    }
    
    /**
     * Sets the UUID of the attendant of the Visitor.
     * 
     * @param attendant UUID of the attendant
     */
    public void setAttendant(UUID attendant) {
        
        this.attendant = attendant;
        
    }
    
    /**
     * Returns the timestamp of visit of the Visitor.
     * 
     * @return Timestamp of visit
     */
    public Timestamp getTimestamp() {
        
        return timestamp;
        
    }
    
    /**
     * Sets the timestamp of visit of the Visitor.
     * 
     * @param timestamp Timestamp of visit
     */
    public void setTimestamp(Timestamp timestamp) {
        
        this.timestamp = timestamp;
        
    }
    
}
