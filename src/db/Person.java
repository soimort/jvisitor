package db;

import java.util.UUID;

/**
 * Person class (abstract).
 * 
 * @author	Mort Yao
 * @since	2012-05-25
 */
public abstract class Person {
    
    /**
     * UUID of the Person.
     */
    protected UUID uuId;
    
    /**
     * Name of the Person.
     */
    protected String name;
    
    /**
     * ID type of the Person.
     */
    protected String idType;
    
    /**
     * ID number of the Person.
     */
    protected String idNumber;
    
    /**
     * Creates a new Person.
     */
    public Person() {
        
    }
    
    /**
     * Returns the UUID of the Person.
     * 
     * @return UUID
     */
    public UUID getUuId() {
        
        return uuId;
        
    }
    
    /**
     * Sets the UUID of the Person.
     * 
     * @param uuId UUID
     */
    public void setUuId(UUID uuId) {
        
        this.uuId = uuId;
        
    }
    
    /**
     * Returns the name of the Person.
     * 
     * @return Name
     */
    public String getName() {
        
        return name;
        
    }
    
    /**
     * Sets the name of the Person.
     * 
     * @param name Name
     */
    public void setName(String name) {
        
        this.name = name;
        
    }
    
    /**
     * Returns the ID type of the Person.
     * 
     * @return ID type
     */
    public String getIdType() {
        
        return idType;
        
    }
    
    /**
     * Sets the ID type of the Person.
     * 
     * @param idType ID type
     */
    public void setIdType(String idType) {
        
        this.idType = idType;
        
    }
    
    /**
     * Returns the ID number of the Person.
     * 
     * @return ID number
    */
    public String getIdNumber() {
        
        return idNumber;
        
    }
    
    /**
     * Sets the ID number of the Person.
     * 
     * @param idNumber ID number
     */
    public void setIdNumber(String idNumber) {
        
        this.idNumber = idNumber;
        
    }
    
}
