package db;

import java.sql.*;
import java.util.ArrayList;

/**
 * Database handler class.
 * 
 * @author  Mort Yao
 * @since   2012-06-10
 * @see Staff
 * @see Visitor
 */
public final class Database {
    
    /**
     * JDBC connection.
     */
    private Connection connection;
    
    /**
     * Staff.
     *
     * @deprecated
     */
    private ArrayList <Staff> staff;
    
    /**
     * Visitors.
     *
     * @deprecated
     */
    private ArrayList <Visitor> visitors;
    
    /**
     * Creates a new database (use default parameters).
     *
     * @deprecated
     */
    public Database() {
        
        try {
            
            Class.forName("org.h2.Driver");
            
            connection = DriverManager.getConnection(
                "jdbc:h2:db/" +
                "jvisitor" +
                ";CACHE_SIZE=" + 16384 +
                ";LOG=" + 2 +
                ";MULTI_THREADED=" + 0 +
                ";QUERY_TIMEOUT=" + 0,
                "",
                "");
            
            initScript(16384, 2, 0, 0);
            
        } catch (Exception e) {
            
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(1);
            
        } finally {
            
            staff = new ArrayList <Staff> ();
            visitors = new ArrayList <Visitor> ();
            
        }
        
    }
    
    /**
     * Creates a new database (with parameters).
     * 
     * @param databaseName Name of the database
     * @param loginName Login name
     * @param password Password
     *
     * @deprecated
     */
    public Database(String databaseName, String loginName, String password) {
        
        try {
            
            Class.forName("org.h2.Driver");
            
            connection = DriverManager.getConnection(
                "jdbc:h2:db/" +
                databaseName +
                ";CIPHER=AES" +
                ";CACHE_SIZE=" + 16384 +
                ";LOG=" + 2 +
                ";MULTI_THREADED=" + 0 +
                ";QUERY_TIMEOUT=" + 0,
                loginName,
                password);
            
            initScript(16384, 2, 0, 0);
            
        } catch (Exception e) {
            
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(1);
            
        } finally {
            
            staff = new ArrayList <Staff> ();
            visitors = new ArrayList <Visitor> ();
            
        }
        
    }
    
    /**
     * Creates a new database (with parameters).
     * 
     * @param databaseName Name of the database
     * @param loginName Login name
     * @param userPassword User password
     * @param filePassword File encryption password
     */
    public Database(String databaseName, String loginName, String userPassword, String filePassword) {
        
        try {
            
            Class.forName("org.h2.Driver");
            
            connection = DriverManager.getConnection(
                "jdbc:h2:db/" +
                databaseName +
                ";CIPHER=AES" +
                ";CACHE_SIZE=" + 16384 +
                ";LOG=" + 2 +
                ";MULTI_THREADED=" + 0 +
                ";QUERY_TIMEOUT=" + 0,
                loginName,
                filePassword + " " + userPassword);
            
            initScript(16384, 2, 0, 0);
            
        } catch (Exception e) {
            
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(1);
            
        } finally {
            
            staff = new ArrayList <Staff> ();
            visitors = new ArrayList <Visitor> ();
            
        }
        
    }
    
    /**
     * Creates a new database (with parameters).
     * 
     * @param databaseName Name of the database
     * @param loginName Login name
     * @param userPassword User password
     * @param filePassword File encryption password
     * @param fileEncryption File encryption algorithm [AES|XTEA]
     * @param fileLock File lock method {FILE|SOCKET|NO}
     * @param ifExists Open the database only if it exists
     */
    public Database(String databaseName, String loginName, String userPassword, String filePassword, String fileEncryption, String fileLock, boolean ifExists) {
        
        try {
            
            Class.forName("org.h2.Driver");
            
            connection = DriverManager.getConnection(
                "jdbc:h2:db/" +
                databaseName +
                (fileEncryption == "AES" || fileEncryption == "XTEA" ?
                    (";CIPHER=" + fileEncryption) :
                    ("")) +
                (fileLock == "FILE" || fileLock == "SOCKET" || fileLock == "NO" ?
                    (";FILE_LOCK=" + fileLock) :
                    ("")) +
                (ifExists ?
                    (";IFEXISTS=TRUE") :
                    ("")
                    ) +
                ";CACHE_SIZE=" + 16384 +
                ";LOG=" + 2 +
                ";MULTI_THREADED=" + 0 +
                ";QUERY_TIMEOUT=" + 0,
                loginName,
                filePassword + " " + userPassword);
            
            initScript(16384, 2, 0, 0);
            
        } catch (Exception e) {
            
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(1);
            
        } finally {
            
            staff = new ArrayList <Staff> ();
            visitors = new ArrayList <Visitor> ();
            
        }
        
    }
    
    /**
     * Creates a new database (with parameters).
     * 
     * @param databaseName Name of the database
     * @param loginName Login name
     * @param userPassword User password
     * @param filePassword File encryption password
     * @param fileEncryption File encryption algorithm [AES|XTEA]
     * @param fileLock File lock method {FILE|SOCKET|NO}
     * @param ifExists Open the database only if it exists
     * @param cacheSize Size of the cache in KB
     * @param log Transaction log mode {0|1|2}
     * @param multiThreaded Enabled (1) or disabled (0) multi-threading inside the database engine {0|1}
     * @param queryTimeout Query timeout of the current session to the given value
     */
    public Database(String databaseName, String loginName, String userPassword, String filePassword, String fileEncryption, String fileLock, boolean ifExists, int cacheSize, int log, int multiThreaded, int queryTimeout) {
        
        try {
            
            Class.forName("org.h2.Driver");
            
            connection = DriverManager.getConnection(
                "jdbc:h2:db/" +
                databaseName +
                (fileEncryption == "AES" || fileEncryption == "XTEA" ?
                    (";CIPHER=" + fileEncryption) :
                    ("")) +
                (fileLock == "FILE" || fileLock == "SOCKET" || fileLock == "NO" ?
                    (";FILE_LOCK=" + fileLock) :
                    ("")) +
                (ifExists ?
                    (";IFEXISTS=TRUE") :
                    ("")
                    ) +
                ";CACHE_SIZE=" + cacheSize +
                ";LOG=" + log +
                ";MULTI_THREADED=" + multiThreaded +
                ";QUERY_TIMEOUT=" + queryTimeout,
                loginName,
                filePassword + " " + userPassword);
            
            initScript(16384, 2, 0, 0);
            
        } catch (Exception e) {
            
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(1);
            
        } finally {
            
            staff = new ArrayList <Staff> ();
            visitors = new ArrayList <Visitor> ();
            
        }
        
    }
    
    /**
     * Returns the name of the current database.
     * 
     * @return Name of the current database
     */
    public String getName() throws SQLException {
        
        String result = "";
        
        PreparedStatement ps = connection.prepareStatement("CALL DATABASE()");
        
        try {
            
            ResultSet rs = ps.executeQuery();
            
            try {
                
                rs.next();
                result = rs.getString(1);
                
            } finally {
                
                try {
                    
                    rs.close();
                    
                } catch (Throwable ignore) {
                    
                    // ignore
                    
                }
            }
            
        } finally {
            
            try {
                
                ps.close();
                
            } catch (Throwable ignore) {
                
                // ignore
                
            }
            
        }
        
        return result;
        
    }
    
    /**
     * Returns the path of the current database.
     * 
     * @return Path of the current database
     */
    public String getPath() throws SQLException {
        
        String result = "";
        
        PreparedStatement ps = connection.prepareStatement("CALL DATABASE_PATH()");
        
        try {
            
            ResultSet rs = ps.executeQuery();
            
            try {
                
                rs.next();
                result = rs.getString(1);
                
            } finally {
                
                try {
                    
                    rs.close();
                    
                } catch (Throwable ignore) {
                    
                    // ignore
                    
                }
            }
            
        } finally {
            
            try {
                
                ps.close();
                
            } catch (Throwable ignore) {
                
                // ignore
                
            }
            
        }
        
        return result;
        
    }
    
    /**
     * Returns the size of the cache in KB for the current database.
     * 
     * @return Size of the cache in KB
     */
    public int getCacheSize() throws SQLException {
        
        int result = 16384;
        
        PreparedStatement ps = connection.prepareStatement("SELECT VALUE FROM INFORMATION_SCHEMA.SETTINGS WHERE NAME = ?");
        ps.setString(1, "CACHE_SIZE");
        
        try {
            
            ResultSet rs = ps.executeQuery();
            
            try {
                
                rs.next();
                result = rs.getInt(1);
                
            } finally {
                
                try {
                    
                    rs.close();
                    
                } catch (Throwable ignore) {
                    
                    // ignore
                    
                }
            }
            
        } finally {
            
            try {
                
                ps.close();
                
            } catch (Throwable ignore) {
                
                // ignore
                
            }
            
        }
        
        return result;
        
    }
    
    /**
     * Sets the size of the cache in KB for the current database.
     * 
     * @param cacheSize Size of the cache in KB
     */
    public void setCacheSize(int cacheSize) throws SQLException {
        
        Statement stmt = connection.createStatement();
        
        try {
            
            int rc = stmt.executeUpdate("SET CACHE_SIZE " + cacheSize);
            System.out.println(">> Setting applied. Operation returns: " + rc);
            
        } finally {
            
            try {
                
                stmt.close();
                
            } catch (Throwable ignore) {
                
                // ignore
                
            }
            
        }
        
    }
    
    /**
     * Returns the transaction log mode.
     * 
     * @return Transaction log mode
     */
    public int getLog() throws SQLException {
        
        int result = 2;
        
        PreparedStatement ps = connection.prepareStatement("SELECT VALUE FROM INFORMATION_SCHEMA.SETTINGS WHERE NAME = ?");
        ps.setString(1, "LOG");
        
        try {
            
            ResultSet rs = ps.executeQuery();
            
            try {
                
                rs.next();
                result = rs.getInt(1);
                
            } finally {
                
                try {
                    
                    rs.close();
                    
                } catch (Throwable ignore) {
                    
                    // ignore
                    
                }
            }
            
        } finally {
            
            try {
                
                ps.close();
                
            } catch (Throwable ignore) {
                
                // ignore
                
            }
            
        }
        
        return result;
        
    }
    
    /**
     * Sets the transaction log mode. The values 0, 1, and 2 are supported.
     * 
     * @param log Transaction log mode
     */
    public void setLog(int log) throws SQLException {
        
        Statement stmt = connection.createStatement();
        
        try {
            
            int rc = stmt.executeUpdate("SET LOG " + log);
            System.out.println(">> Setting applied. Operation returns: " + rc);
            
        } finally {
            
            try {
                
                stmt.close();
                
            } catch (Throwable ignore) {
                
                // ignore
                
            }
            
        }
        
    }
    
    /**
     * Returns whether multi-threading is enabled (1) or disabled (0) inside the database engine.
     * 
     * @return Enabled (1) or disabled (0) multi-threading inside the database engine
     */
    public int getMultiThreaded() throws SQLException {
        
        int result = 0;
        
        PreparedStatement ps = connection.prepareStatement("SELECT VALUE FROM INFORMATION_SCHEMA.SETTINGS WHERE NAME = ?");
        ps.setString(1, "MULTI_THREADED");
        
        try {
            
            ResultSet rs = ps.executeQuery();
            
            try {
                
                rs.next();
                result = rs.getInt(1);
                
            } finally {
                
                try {
                    
                    rs.close();
                    
                } catch (Throwable ignore) {
                    
                    // ignore
                    
                }
            }
            
        } finally {
            
            try {
                
                ps.close();
                
            } catch (Throwable ignore) {
                
                // ignore
                
            }
            
        }
        
        return result;
        
    }
    
    /**
     * Enabled (1) or disabled (0) multi-threading inside the database engine.
     * 
     * @param multiThreaded Enabled (1) or disabled (0) multi-threading inside the database engine
     */
    public void setMultiThreaded(int multiThreaded) throws SQLException {
        
        Statement stmt = connection.createStatement();
        
        try {
            
            int rc = stmt.executeUpdate("SET MULTI_THREADED " + multiThreaded);
            System.out.println(">> Setting applied. Operation returns: " + rc);
            
        } finally {
            
            try {
                
                stmt.close();
                
            } catch (Throwable ignore) {
                
                // ignore
                
            }
            
        }
        
    }
    
    /**
     * Returns the query timeout of the current session to the given value. The timeout is in milliseconds.
     * 
     * @return Query timeout of the current session to the given value
     */
    public int getQueryTimeout() throws SQLException {
        
        int result = 0;
        
        PreparedStatement ps = connection.prepareStatement("SELECT VALUE FROM INFORMATION_SCHEMA.SETTINGS WHERE NAME = ?");
        ps.setString(1, "QUERY_TIMEOUT");
        
        try {
            
            ResultSet rs = ps.executeQuery();
            
            try {
                
                rs.next();
                result = rs.getInt(1);
                
            } finally {
                
                try {
                    
                    rs.close();
                    
                } catch (Throwable ignore) {
                    
                    // ignore
                    
                }
            }
            
        } finally {
            
            try {
                
                ps.close();
                
            } catch (Throwable ignore) {
                
                // ignore
                
            }
            
        }
        
        return result;
        
    }
    
    /**
     * Sets the query timeout of the current session to the given value. The timeout is in milliseconds.
     * 
     * @param queryTimeout Query timeout of the current session to the given value
     */
    public void setQueryTimeout(int queryTimeout) throws SQLException {
        
        Statement stmt = connection.createStatement();
        
        try {
            
            int rc = stmt.executeUpdate("SET QUERY_TIMEOUT " + queryTimeout);
            System.out.println(">> Setting applied. Operation returns: " + rc);
            
        } finally {
            
            try {
                
                stmt.close();
                
            } catch (Throwable ignore) {
                
                // ignore
                
            }
            
        }
        
    }
    
    /**
     * Changes the password of the current user.
     * 
     * @param password New password
     */
    public void setPassword(String password) throws SQLException {
        
        Statement stmt = connection.createStatement();
        
        try {
            
            int rc = stmt.executeUpdate("SET PASSWORD \'" + password + "\'");
            System.out.println(">> Setting applied. Operation returns: " + rc);
            
        } finally {
            
            try {
                
                stmt.close();
                
            } catch (Throwable ignore) {
                
                // ignore
                
            }
            
        }
        
    }
    
    /**
     * Run the initialization script if it is a new database.
     *
     * @param cacheSize Size of the cache in KB
     * @param log Transaction log mode {0|1|2}
     * @param multiThreaded Enabled (1) or disabled (0) multi-threading inside the database engine {0|1}
     * @param queryTimeout Query timeout of the current session to the given value
     */
    private void initScript(int cacheSize, int log, int multiThreaded, int queryTimeout) throws SQLException {
        
        PreparedStatement ps = connection.prepareStatement("SHOW TABLES FROM PUBLIC");
        
        ResultSet rs = ps.executeQuery();
        
        if (!rs.next()) {
            
            Statement stmt = connection.createStatement();
            try {
                
                int rc = stmt.executeUpdate("" +
                    "SET CACHE_SIZE " + cacheSize + ";" +
                    "SET LOG " + log + ";" +
                    "SET MULTI_THREADED " + multiThreaded + ";" +
                    "SET QUERY_TIMEOUT " + queryTimeout + ";" +
        	    "CREATE TABLE Staff(" +
                    "S_Id UUID PRIMARY KEY," +
                    "Name VARCHAR(255) NOT NULL," +
                    "IdType VARCHAR(255) DEFAULT 'PERSONNUMMER'," +
                    "IdNumber VARCHAR(255) DEFAULT ''," +
                    ");" +
                    "CREATE TABLE Individuals(" +
                    "I_Id UUID PRIMARY KEY," +
                    "Name VARCHAR(255) NOT NULL," +
                    "IdType VARCHAR(255) DEFAULT 'PERSONNUMMER'," +
                    "IdNumber VARCHAR(255) DEFAULT ''," +
                    "Company VARCHAR(255) DEFAULT ''," +
                    "Country VARCHAR(255) DEFAULT ''," +
                    ");" +
                    "CREATE TABLE Visitors(" +
                    "I_Id UUID PRIMARY KEY," +
                    "Attendant UUID," +
                    "DateTime TIMESTAMP," +
                    ");");
                System.out.println(">> Database initialized. Operation returns: " + rc);
                
            } finally {
                
                try {
                    
                    stmt.close();
                    
                } catch (Throwable ignore) {
                    
                    // ignore
                    
                }
                
            }
            
        }
        
    }
    
    /**
     * Closes the connection when finalizing the database object.
     */
    protected void finalize() {
        
        try {
            
            connection.close();
            
        } catch (Throwable ignore) {
            
            // ignore
            
        }
        
    }
    
    /**
     * Returns an array containing all staff in the database.
     * 
     * @return Array containing all staff
     * @throws SQLException
     */
    public Staff[] getStaffAll() throws SQLException {
        
        ArrayList <Staff> staff = new ArrayList <Staff> ();
        
        PreparedStatement ps = connection.prepareStatement("SELECT * FROM Staff");

        selectStaffFromDatabase(staff, ps);
        
        return staff.toArray(new Staff[staff.size()]);
        
    }
    
    /**
     * Returns an array containing all matched staff by searching name.
     * 
     * @param pattern Pattern of name
     * @return Array containing all matched staff
     * @throws SQLException
     */
    public Staff[] getStaffByName(String pattern) throws SQLException {
        
        ArrayList <Staff> staff = new ArrayList <Staff> ();
        
        PreparedStatement ps = connection.prepareStatement("SELECT * FROM Staff WHERE upper(Name) LIKE upper( ? )");
        ps.setString(1, pattern + "%");
        
        selectStaffFromDatabase(staff, ps);
        
        return staff.toArray(new Staff[staff.size()]);
        
    }
    
    /**
     * Returns an array containing all matched staff by searching ID type.
     * 
     * @param pattern Pattern of ID type
     * @return Array containing all matched staff
     * @throws SQLException
     */
    public Staff[] getStaffByIdType(String pattern) throws SQLException {
        
        ArrayList <Staff> staff = new ArrayList <Staff> ();
        
        PreparedStatement ps = connection.prepareStatement("SELECT * FROM Staff WHERE IdType = ?");
        ps.setString(1, pattern);
        
        selectStaffFromDatabase(staff, ps);
        
        return staff.toArray(new Staff[staff.size()]);
        
    }
    
    /**
     * Returns an array containing all matched staff by searching ID number.
     * 
     * @param pattern Pattern of ID number
     * @return Array containing all matched staff
     * @throws SQLException
     */
    public Staff[] getStaffByIdNumber(String pattern) throws SQLException {
        
        ArrayList <Staff> staff = new ArrayList <Staff> ();
        
        PreparedStatement ps = connection.prepareStatement("SELECT * FROM Staff WHERE upper(IdNumber) LIKE upper( ? )");
        ps.setString(1, pattern + "%");
        
        selectStaffFromDatabase(staff, ps);
        
        return staff.toArray(new Staff[staff.size()]);
        
    }
    
    /**
     * Returns an array containing all visitors in the database.
     * 
     * @return Array containing all visitors
     * @throws SQLException
     */
    public Visitor[] getVisitorsAll() throws SQLException {
        
        ArrayList <Visitor> visitors = new ArrayList <Visitor> ();
        
        PreparedStatement ps = connection.prepareStatement("SELECT * FROM Individuals, Visitors WHERE Individuals.I_Id = Visitors.I_Id");
        
        selectVisitorsFromDatabase(visitors, ps);
        
        return visitors.toArray(new Visitor[visitors.size()]);
        
    }
    
    /**
     * Returns an array containing all matched visitors by searching name.
     * 
     * @param pattern Pattern of name
     * @return Array containing all matched visitors
     * @throws SQLException
     */
    public Visitor[] getVisitorsByName(String pattern) throws SQLException {
        
        ArrayList <Visitor> visitors = new ArrayList <Visitor> ();
        
        PreparedStatement ps = connection.prepareStatement("SELECT * FROM Individuals, Visitors WHERE Individuals.I_Id = Visitors.I_Id AND upper(Name) LIKE upper( ? )");
        ps.setString(1, pattern + "%");
        
        selectVisitorsFromDatabase(visitors, ps);
        
        return visitors.toArray(new Visitor[visitors.size()]);
        
    }
    
    /**
     * Returns an array containing all matched visitors by searching ID type.
     * 
     * @param pattern Pattern of ID type
     * @return Array containing all matched visitors
     * @throws SQLException
     */
    public Visitor[] getVisitorsByIdType(String pattern) throws SQLException {
        
        ArrayList <Visitor> visitors = new ArrayList <Visitor> ();
        
        PreparedStatement ps = connection.prepareStatement("SELECT * FROM Individuals, Visitors WHERE Individuals.I_Id = Visitors.I_Id AND IdType = ?");
        ps.setString(1, pattern);
        
        selectVisitorsFromDatabase(visitors, ps);
        
        return visitors.toArray(new Visitor[visitors.size()]);
        
    }
    
    /**
     * Returns an array containing all matched visitors by searching ID number.
     * 
     * @param pattern Pattern of ID number
     * @return Array containing all matched visitors
     * @throws SQLException
     */
    public Visitor[] getVisitorsByIdNumber(String pattern) throws SQLException {
        
        ArrayList <Visitor> visitors = new ArrayList <Visitor> ();
        
        PreparedStatement ps = connection.prepareStatement("SELECT * FROM Individuals, Visitors WHERE Individuals.I_Id = Visitors.I_Id AND upper(IdNumber) LIKE upper( ? )");
        ps.setString(1, pattern + "%");
        
        selectVisitorsFromDatabase(visitors, ps);
        
        return visitors.toArray(new Visitor[visitors.size()]);
        
    }
    
    /**
     * Returns an array containing all matched visitors by searching company.
     * 
     * @param pattern Pattern of company
     * @return Array containing all matched visitors
     * @throws SQLException
     */
    public Visitor[] getVisitorsByCompany(String pattern) throws SQLException {
        
        ArrayList <Visitor> visitors = new ArrayList <Visitor> ();
        
        PreparedStatement ps = connection.prepareStatement("SELECT * FROM Individuals, Visitors WHERE Individuals.I_Id = Visitors.I_Id AND upper(Company) LIKE upper( ? )");
        ps.setString(1, pattern + "%");
        
        selectVisitorsFromDatabase(visitors, ps);
        
        return visitors.toArray(new Visitor[visitors.size()]);
        
    }
    
    /**
     * Returns an array containing all matched visitors by searching country.
     * 
     * @param pattern Pattern of country
     * @return Array containing all matched visitors
     * @throws SQLException
     */
    public Visitor[] getVisitorsByCountry(String pattern) throws SQLException {
        
        ArrayList <Visitor> visitors = new ArrayList <Visitor> ();
        
        PreparedStatement ps = connection.prepareStatement("SELECT * FROM Individuals, Visitors WHERE Individuals.I_Id = Visitors.I_Id AND upper(Country) LIKE upper( ? )");
        ps.setString(1, pattern + "%");
        
        selectVisitorsFromDatabase(visitors, ps);
        
        return visitors.toArray(new Visitor[visitors.size()]);
        
    }
    
    /**
     * Returns an array containing all matched visitors by searching date of visit.
     * 
     * @param pattern Pattern of date of visit
     * @return Array containing all matched visitors
     * @throws SQLException
     */
    public Visitor[] getVisitorsByDate(String pattern) throws SQLException {
        
        ArrayList <Visitor> visitors = new ArrayList <Visitor> ();
        
        PreparedStatement ps = connection.prepareStatement("SELECT * FROM Individuals, Visitors WHERE Individuals.I_Id = Visitors.I_Id AND DateTime LIKE ?");
        ps.setString(1, pattern + "%");
        
        selectVisitorsFromDatabase(visitors, ps);
        
        return visitors.toArray(new Visitor[visitors.size()]);
        
    }
    
    /**
     * Adds an existing Staff object to the database.
     * 
     * @param newStaff Staff to be added
     * @throws SQLException
     */
    public void addStaff(Staff newStaff) throws SQLException {
        
        staff.add(newStaff);
        
        addStaffToDatabase(newStaff);
        
    }
    
    /**
     * Creates a new Staff object and adds it to the database.
     * 
     * @param name Name of the Staff
     * @return Added Staff
     * @throws SQLException
     *
     * @deprecated
     */
    public Staff addNewStaff(String name) throws SQLException {
        
        Staff newStaff = new Staff(name);
        staff.add(newStaff);
        
        addStaffToDatabase(newStaff);
        
        return newStaff;
        
    }
    
    /**
     * Creates a new Staff object and adds it to the database.
     * 
     * @param name Name of the Staff
     * @param idType ID type of the Staff
     * @param idNumber ID number of the Staff
     * @return Added Staff
     * @throws SQLException
     */
    public Staff addNewStaff(String name, String idType, String idNumber) throws SQLException {
        
        Staff newStaff = new Staff(name, idType, idNumber);
        staff.add(newStaff);
        
        addStaffToDatabase(newStaff);
        
        return newStaff;
        
    }
    
    /**
     * Removes a Staff from the database.
     * 
     * @param deletableStaff Staff to be removed
     * @throws SQLException
     */
    public void removeStaff(Staff deletableStaff) throws SQLException {
        
        removeStaffFromDatabase(deletableStaff);
        
        staff.remove(deletableStaff);
        
    }
    
    /**
     * Adds an existing Visitor object to the database.
     * 
     * @param newVisitor Visitor to be added
     * @throws SQLException
     */
    public void addVisitor(Visitor newVisitor) throws SQLException {
        
        visitors.add(newVisitor);
        
        addVisitorToDatabase(newVisitor);
        
    }
    
    /**
     * Creates a new Visitor object and adds it to the database.
     * 
     * @param name Name of the Visitor
     * @param idType ID type of the Visitor
     * @param idNumber ID number of the Visitor
     * @param company Company of the Visitor
     * @param country Country of the Visitor
     * @param attendantStaff Attendant (Staff) of the Visitor
     * @return Added Visitor
     * @throws SQLException
     */
    public Visitor addNewVisitor(String name, String idType, String idNumber, String company, String country, Staff attendantStaff) throws SQLException {
        
        Visitor newVisitor = new Visitor(name, idType, idNumber, company, country, attendantStaff);
        visitors.add(newVisitor);
        
        addVisitorToDatabase(newVisitor);
        
        return newVisitor;
        
    }
    
    /**
     * Removes a Visitor from the database.
     * 
     * @param deletableVisitor Visitor to be deleted
     * @throws SQLException
     */
    public void removeVisitor(Visitor deletableVisitor) throws SQLException {
        
        removeVisitorFromDatabase(deletableVisitor);
        
        visitors.remove(deletableVisitor);
        
    }
    
    /**
     * Adds a Staff to the database.
     * 
     * @param newStaff Staff to be added
     * @throws SQLException
     */
    private void addStaffToDatabase(Staff newStaff) throws SQLException {
        
        Statement stmt = connection.createStatement();
        try {
            
            int rc = stmt.executeUpdate("INSERT INTO Staff (S_Id, Name, IdType, IdNumber) "
                    + "VALUES ('" + newStaff.getUuId()
                    + "', '" + newStaff.getName()
                    + "', '" + newStaff.getIdType()
                    + "', '" + newStaff.getIdNumber()
                    + "')");
            
            System.out.println(">> Staff added to the database. Operation returns: " + rc);
            
        } finally {
            
            try {
                
                stmt.close();
                
            } catch (Throwable ignore) {
                
                // ignore
                
            }
            
        }
        
    }
    
    /**
     * Adds a Visitor to the database.
     * 
     * @param newVisitor Visitor to be added
     * @throws SQLException
     */
    private void addVisitorToDatabase(Visitor newVisitor) throws SQLException {
        
        Statement stmt = connection.createStatement();
        try {
            
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM Individuals WHERE I_Id = ?");
            ps.setString(1, newVisitor.getUuId().toString());
            
            ResultSet rs = ps.executeQuery();
            if (!rs.next()) {
                
                stmt.executeUpdate("INSERT INTO Individuals (I_Id, Name, IdType, IdNumber, Company, Country) "
                    + "VALUES ('" + newVisitor.getUuId()
                    + "', '" + newVisitor.getName()
                    + "', '" + newVisitor.getIdType()
                    + "', '" + newVisitor.getIdNumber()
                    + "', '" + newVisitor.getCompany()
                    + "', '" + newVisitor.getCountry()
                    + "')");
                
            }
            
            int rc = stmt.executeUpdate("INSERT INTO Visitors (I_Id, Attendant, DateTime) "
                    + "VALUES ('" + newVisitor.getUuId()
                    + "', '" + newVisitor.getAttendant()
                    + "', CURRENT_TIMESTAMP)");
            
            System.out.println(">> Visitor added to the database. Operation returns: " + rc);
            
        } finally {
            
            try {
                
                stmt.close();
                
            } catch (Throwable ignore) {
                
                // ignore
                
            }
            
        }
        
    }
    
    /**
     * Removes a Staff from the database.
     * 
     * @param deletableStaff Staff to be removed
     * @throws SQLException
     */
    private void removeStaffFromDatabase(Staff deletableStaff) throws SQLException {
        
        Statement stmt = connection.createStatement();
        try {
            
            int rc = stmt.executeUpdate("DELETE FROM Staff WHERE S_Id = \'"
                    + deletableStaff.getUuId().toString() + "\'");
            
            System.out.println(">> Staff removed from the database. Operation returns: " + rc);
            
        } finally {
            
            try {
                
                stmt.close();
                
            } catch (Throwable ignore) {
                
                // ignore
                
            }
            
        }
        
    }
    
    /**
     * Removes a Visitor from the database.
     * 
     * @param deletableVisitor Visitor to be removed
     * @throws SQLException
     */
    private void removeVisitorFromDatabase(Visitor deletableVisitor) throws SQLException {
        
        Statement stmt = connection.createStatement();
        try {
            
            int rc = stmt.executeUpdate("DELETE FROM Visitors WHERE I_Id = \'"
                    + deletableVisitor.getUuId().toString()
                    + "\' AND Attendant = \'"
                    + deletableVisitor.getAttendant().toString()
                    + "\' AND DateTime = \'"
                    + deletableVisitor.getTimestamp().toString() + "\'");
            
            System.out.println(">> Visitor removed from the database. Operation returns: " + rc);
            
        } finally {
            
            try {
                
                stmt.close();
                
            } catch (Throwable ignore) {
                
                // ignore
                
            }
            
        }
        
    }
    
    /**
     * Selects staff from the database.
     * 
     * @param staff ArrayList of staff
     * @param ps PreparedStatement
     * @throws SQLException
     */
    private void selectStaffFromDatabase(ArrayList <Staff> staff, PreparedStatement ps) throws SQLException {
        
        try {
            
            ResultSet rs = ps.executeQuery();
            
            try {
                
                while (rs.next()) {
                    
                    staff.add(new Staff(rs.getString(1), // S_Id
                            rs.getString(2), // Name
                            rs.getString(3), // IdType
                            rs.getString(4))); // IdNumber
                    
                }
                
            } finally {
                
                try {
                    
                    rs.close();
                    
                } catch (Throwable ignore) {
                    
                    // ignore
                    
                }
            }
            
        } finally {
            
            try {
                
                ps.close();
                
            } catch (Throwable ignore) {
                
                // ignore
                
            }
            
        }
        
    }
    
    /**
     * Selects visitors from the database.
     * 
     * @param visitors ArrayList of visitors
     * @param ps PreparedStatement
     * @throws SQLException
     */
    private void selectVisitorsFromDatabase(ArrayList <Visitor> visitors, PreparedStatement ps) throws SQLException {
        
        try {
            
            ResultSet rs = ps.executeQuery();
            
            try {
                
                while (rs.next()) {
                    
                    ArrayList <Staff> staff = new ArrayList <Staff> ();
                    
                    PreparedStatement ps2 = connection.prepareStatement("SELECT * FROM Staff WHERE S_Id = ?");
                    ps2.setString(1, rs.getString(8));
                    
                    selectStaffFromDatabase(staff, ps2);
                    
                    if (!staff.isEmpty()) {
                        Staff attendantStaff = staff.toArray(new Staff[staff.size()])[0];
                        
                        visitors.add(new Visitor(rs.getString(1), // I_Id
                                rs.getString(2), // Name
                                rs.getString(3), // IdType
                                rs.getString(4), // IdNumber
                                rs.getString(5), // Company
                                rs.getString(6), // Country
                                attendantStaff, // Attendant
                                rs.getTimestamp(9))); // DateTime
                    }
                    
                }
                
            } finally {
                
                try {
                    
                    rs.close();
                    
                } catch (Throwable ignore) {
                    
                    // ignore
                    
                }
            }
            
        } finally {
            
            try {
                
                ps.close();
                
            } catch (Throwable ignore) {
                
                // ignore
                
            }
            
        }
        
    }
}
