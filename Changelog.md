# 2012-05-25

* database schema normalized;
* getAttendantStaff() can now correctly return a Staff object; (see Demo.java)
* a few changes of methods; (see javadoc for reference)
* the database is now encrypted (using AES). the user name is "sa" and the encryption / user password is "group 12"
    use:
        Database db = new Database("db/jvisitor", "sa", "group 12");
    instead of:
        Database db = new Database();
    to create the database handler; (see Demo.java)
* H2 updated to the newest version (1.3.167).

# 2012-05-28

* getVisitorsAll() bug fixed.

# 2012-05-30

* getVisitorsByDate() updated.

# 2012-05-31

* removeStaff(), removeVisitor() bug fixed.

# 2012-06-01

* getStaffByName(), getVisitorsByName() updated.

# 2012-06-05

* getVisitorsByCompany(), getVisitorsByCountry() updated.

# 2012-06-09

* db/new_db.sql -> init.sql;
* doc/ updated;
* lib/h2-1.3.166.jar removed;
* misc/ removed;
* src/db/Database.java updated;
* src/db/Demo.java added;
* src/db/Administrator.java removed;
* build.xml updated.
